use std::convert::Infallible;
use std::str::FromStr;

#[derive(Clone, Debug, PartialEq, Eq)]
pub(crate) enum Version {
    Semver {
        major: u64,
        minor: u64,
        point: u64,
        extra: Option<String>,
    },
    Abstract(String),
}

pub(crate) trait LatestOrSemver {
    fn latest_or_semver(self, default: Option<Version>) -> Option<Version>;
}

impl<T> LatestOrSemver for T
where
    T: IntoIterator<Item = Version>,
{
    fn latest_or_semver(self, default: Option<Version>) -> Option<Version> {
        prefer_latest_or_semver(default, self)
    }
}

fn prefer_latest_or_semver(
    default: Option<Version>,
    iter: impl IntoIterator<Item = Version>,
) -> Option<Version> {
    iter.into_iter().fold(default, |max, current| {
        if let Some(max) = max {
            if max > current || !current.is_semver() {
                Some(max)
            } else {
                Some(current)
            }
        } else {
            Some(current)
        }
    })
}

impl Version {
    pub(crate) fn parse_from(s: &str) -> Self {
        match s.parse::<Self>() {
            Ok(this) => this,
            Err(e) => match e {},
        }
    }

    pub(crate) fn is_semver(&self) -> bool {
        matches!(self, Self::Semver { .. })
    }
}

impl Default for Version {
    fn default() -> Self {
        Version::Semver {
            major: 0,
            minor: 0,
            point: 0,
            extra: None,
        }
    }
}

impl FromStr for Version {
    type Err = Infallible;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if let Some((major, rest)) = s.trim_start_matches('v').split_once('.') {
            if let Some((minor, rest)) = rest.split_once('.') {
                let (point, rest) = if let Some(index) = rest.find(|c: char| !c.is_ascii_digit()) {
                    let (point, rest) = rest.split_at(index);

                    (point, Some(rest.to_string()))
                } else {
                    (rest, None)
                };

                let res = [major, minor, point]
                    .into_iter()
                    .filter_map(|s| s.parse::<u64>().ok())
                    .collect::<Vec<_>>()
                    .try_into();

                if let Ok(arr) = res {
                    let arr: [u64; 3] = arr;

                    return Ok(Version::Semver {
                        major: arr[0],
                        minor: arr[1],
                        point: arr[2],
                        extra: rest,
                    });
                }
            }
        }

        Ok(Version::Abstract(s.to_owned()))
    }
}

impl PartialOrd for Version {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        match (self, other) {
            (
                Self::Semver {
                    major: lhs_major,
                    minor: lhs_minor,
                    point: lhs_point,
                    extra: lhs_extra,
                },
                Self::Semver {
                    major,
                    minor,
                    point,
                    extra,
                },
            ) => match lhs_major.partial_cmp(major) {
                Some(std::cmp::Ordering::Equal) => match lhs_minor.partial_cmp(minor) {
                    Some(std::cmp::Ordering::Equal) => match lhs_point.partial_cmp(point) {
                        Some(std::cmp::Ordering::Equal) => lhs_extra.partial_cmp(extra),
                        rest => rest,
                    },
                    rest => rest,
                },
                rest => rest,
            },
            (Self::Abstract(lhs), Self::Abstract(rhs)) => lhs.partial_cmp(rhs),
            _ => None,
        }
    }
}

impl std::fmt::Display for Version {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Semver {
                major,
                minor,
                point,
                extra: Some(extra),
            } => {
                write!(f, "v{}.{}.{}{}", major, minor, point, extra)
            }
            Self::Semver {
                major,
                minor,
                point,
                extra: None,
            } => {
                write!(f, "v{}.{}.{}", major, minor, point)
            }
            Self::Abstract(version) => write!(f, "{}", version),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::{LatestOrSemver, Version};

    #[test]
    fn parse_semver() {
        let semvers = [
            "v0.0.1",
            "v0.1.0",
            "v1.0.0",
            "0.0.1",
            "0.1.0",
            "1.0.0",
            "v0.0.1-r0",
            "v0.1.0-r0",
            "v1.0.0-r0",
            "0.0.1-r0",
            "0.1.0-r0",
            "1.0.0-r0",
        ];

        for semver in semvers {
            println!("Parsing {}", semver);
            assert!(Version::parse_from(semver).is_semver());
        }
    }

    #[test]
    fn to_string() {
        assert_eq!(
            Version::Semver {
                major: 5,
                minor: 8,
                point: 4,
                extra: Some("-r12".into()),
            }
            .to_string(),
            "v5.8.4-r12"
        );
        assert_eq!(
            Version::Semver {
                major: 0,
                minor: 0,
                point: 0,
                extra: None,
            }
            .to_string(),
            "v0.0.0"
        );
    }

    #[test]
    fn round_trip() {
        let semvers = [
            Version::Semver {
                major: 0,
                minor: 0,
                point: 1,
                extra: None,
            },
            Version::Semver {
                major: 0,
                minor: 1,
                point: 0,
                extra: None,
            },
            Version::Semver {
                major: 1,
                minor: 0,
                point: 0,
                extra: None,
            },
            Version::Semver {
                major: 0,
                minor: 0,
                point: 1,
                extra: Some("hi".into()),
            },
            Version::Semver {
                major: 0,
                minor: 1,
                point: 0,
                extra: Some("hi".into()),
            },
            Version::Semver {
                major: 1,
                minor: 0,
                point: 0,
                extra: Some("hi".into()),
            },
        ];

        for semver in semvers {
            let new_semver = Version::parse_from(&semver.to_string());
            assert_eq!(new_semver, semver);
        }
    }

    #[test]
    fn uses_latest_with_no_semver() {
        let items = [
            Version::Abstract("hello".into()),
            Version::Abstract("hi".into()),
            Version::Abstract("howdy".into()),
            Version::Abstract("henlo".into()),
            Version::Abstract("hewwo".into()),
        ];

        let opt = items.latest_or_semver(None);

        assert_eq!(opt, Some(Version::Abstract("hello".into())));
    }

    #[test]
    fn uses_largest_semver() {
        let items = [
            Version::Abstract("hello".into()),
            Version::Abstract("hi".into()),
            Version::Semver {
                major: 2,
                minor: 0,
                point: 0,
                extra: None,
            },
            Version::Abstract("henlo".into()),
            Version::Semver {
                major: 2,
                minor: 1,
                point: 0,
                extra: None,
            },
            Version::Abstract("hewwo".into()),
            Version::Semver {
                major: 2,
                minor: 0,
                point: 1,
                extra: None,
            },
        ];

        let opt = items.latest_or_semver(None);

        assert_eq!(
            opt,
            Some(Version::Semver {
                major: 2,
                minor: 1,
                point: 0,
                extra: None
            })
        );
    }
}

use crate::config::LanguageConfig;
use crate::version::Version;
use crate::BuildDirective;
use std::path::PathBuf;
use time::{macros::format_description, parsing::Parsed, OffsetDateTime};

#[derive(Clone, Debug, serde::Deserialize)]
struct CargoToml {
    package: CargoPackage,
}

#[derive(Clone, Debug, serde::Deserialize)]
struct CargoPackage {
    version: String,
}

struct RemoveOnDrop<'a>(&'a str);

impl<'a> Drop for RemoveOnDrop<'a> {
    fn drop(&mut self) {
        let _ = std::fs::remove_dir_all(self.0);
    }
}

#[derive(Debug)]
enum GitError {
    Branch,
    Clone,
    Date,
}

impl std::fmt::Display for GitError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Branch => write!(f, "Failed to checkout branch"),
            Self::Clone => write!(f, "Failed to clone repository"),
            Self::Date => write!(f, "Failed to fetch date from repository"),
        }
    }
}

impl std::error::Error for GitError {}

#[tracing::instrument]
pub(crate) async fn check_git_project(
    repository: String,
    branch: String,
    recent: i64,
    language: LanguageConfig,
) -> color_eyre::eyre::Result<BuildDirective> {
    static CHECK_REPO: &str = "checked-project";
    let _remover = RemoveOnDrop(CHECK_REPO);

    let output = tokio::process::Command::new("git")
        .args(["clone", &repository, CHECK_REPO])
        .output()
        .await?;

    if !output.status.success() {
        return Err(GitError::Clone.into());
    }

    let output = tokio::process::Command::new("git")
        .args(["checkout", &branch])
        .current_dir(CHECK_REPO)
        .output()
        .await?;

    if !output.status.success() {
        return Err(GitError::Branch.into());
    }

    let output = tokio::process::Command::new("git")
        .args(["show", "-s", "--format=%ci", "HEAD"])
        .current_dir(CHECK_REPO)
        .output()
        .await?;

    if !output.status.success() {
        return Err(GitError::Date.into());
    }

    let time_format = format_description!(
        "[year]-[month]-[day] [hour]:[minute]:[second] [offset_hour][offset_minute]"
    );

    let mut parsed = Parsed::new();
    parsed.parse_items(&output.stdout, time_format)?;
    let date_time: OffsetDateTime = parsed.try_into()?;
    let now = OffsetDateTime::now_utc();

    let duration = now - date_time;
    if duration.whole_days() > recent {
        return Ok(BuildDirective::ShouldIgnore);
    }

    match language {
        LanguageConfig::Rust { config } => {
            let config_path = PathBuf::from(CHECK_REPO).join(config);

            let config_string = tokio::fs::read_to_string(&config_path).await?;

            let rust_config: CargoToml = toml::from_str(&config_string)?;

            let version = Version::parse_from(&rust_config.package.version);

            Ok(BuildDirective::ShouldBuild(Some(version)))
        }
    }
}

use crate::{config::GiteaConfig, revision::Revision};
use reqwest::{
    header::{HeaderMap, HeaderValue},
    Client, StatusCode,
};

#[derive(Clone, Debug, serde::Serialize)]
struct GiteaTag {
    tag_name: String,
}

#[derive(Clone, Debug, serde::Deserialize)]
struct GiteaTagResponse {
    name: String,
}

#[derive(Debug)]
enum TagError {
    Status(StatusCode),
}

impl std::fmt::Display for TagError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Status(code) => {
                write!(f, "Failed to create tag, invalid response status: {}", code)
            }
        }
    }
}

impl std::error::Error for TagError {}

#[tracing::instrument(skip_all)]
pub(crate) fn build_client(gitea_token: &str) -> color_eyre::eyre::Result<Client> {
    let mut headers = HeaderMap::new();
    headers.insert(
        "Authorization",
        HeaderValue::from_str(&format!("token {}", gitea_token))?,
    );

    let client = reqwest::Client::builder()
        .default_headers(headers)
        .user_agent("release-checker (+https://git.asonix.dog/asonix/release-checker)")
        .build()?;

    Ok(client)
}

#[tracing::instrument(skip(client))]
pub(crate) async fn get_previous_revision(
    client: &Client,
    config: &GiteaConfig,
) -> color_eyre::eyre::Result<Revision> {
    let mut last_revision: Option<Revision> = None;
    const LIMIT: usize = 10;

    // Iterate through all pages
    for page_num in 1.. {
        let tags_url = format!(
            "https://{}/api/v1/repos/{}/{}/tags?page={}&limit={}",
            config.domain, config.owner, config.repo, page_num, LIMIT
        );
        let tags_response = client.get(tags_url).send().await?;

        let tags: Vec<GiteaTagResponse> = tags_response.json().await?;

        let should_break = tags.len() < LIMIT;

        last_revision = tags.into_iter().map(|tag| tag.name.parse().ok()).fold(
            last_revision,
            |max, current| {
                if let Some(current) = current {
                    if let Some(max) = max {
                        if current > max {
                            Some(current)
                        } else {
                            Some(max)
                        }
                    } else if current.version.is_semver() {
                        Some(current)
                    } else {
                        None
                    }
                } else {
                    max
                }
            },
        );

        if should_break {
            break;
        }
    }

    Ok(last_revision.unwrap_or_default())
}

#[tracing::instrument(skip(client))]
pub(crate) async fn create_tag(
    client: &Client,
    config: &GiteaConfig,
    revision: &Revision,
) -> color_eyre::eyre::Result<()> {
    let tag_url = format!(
        "https://{}/api/v1/repos/{}/{}/tags",
        config.domain, config.owner, config.repo
    );

    let response = client
        .post(tag_url)
        .json(&GiteaTag {
            tag_name: revision.to_string(),
        })
        .send()
        .await?;

    if !response.status().is_success() {
        return Err(TagError::Status(response.status()).into());
    }

    Ok(())
}

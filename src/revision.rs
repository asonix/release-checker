use crate::version::Version;
use std::{num::ParseIntError, str::FromStr};

#[derive(Clone, Debug, Default, PartialEq, Eq)]
pub(crate) struct Revision {
    pub(crate) version: Version,
    pub(crate) revision: usize,
}

#[derive(Debug)]
pub(crate) enum RevisionError {
    Revision(ParseIntError),
}

impl Revision {
    pub(crate) fn next(self, version: Option<Version>) -> Self {
        if let Some(version) = version {
            if self.version == version {
                Revision {
                    revision: self.revision + 1,
                    ..self
                }
            } else {
                Revision {
                    version,
                    revision: 0,
                }
            }
        } else {
            Revision {
                revision: self.revision + 1,
                ..self
            }
        }
    }
}

impl FromStr for Revision {
    type Err = RevisionError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if let Some((version, revision)) = s.rsplit_once('-') {
            let revision = revision
                .trim_start_matches('r')
                .parse()
                .map_err(RevisionError::Revision)?;

            Ok(Revision {
                version: Version::parse_from(version),
                revision,
            })
        } else {
            Ok(Revision {
                version: Version::parse_from(s),
                revision: 0,
            })
        }
    }
}

impl PartialOrd for Revision {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        match self.version.partial_cmp(&other.version) {
            Some(std::cmp::Ordering::Equal) => self.revision.partial_cmp(&other.revision),
            rest => rest,
        }
    }
}

impl std::fmt::Display for Revision {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}-r{}", self.version, self.revision)
    }
}

impl std::fmt::Display for RevisionError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Revision(e) => write!(f, "Failed to parse revision number, {}", e),
        }
    }
}

impl std::error::Error for RevisionError {}

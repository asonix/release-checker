use crate::{
    revision::Revision,
    version::{LatestOrSemver, Version},
    BuildDirective,
};

#[derive(serde::Deserialize)]
struct Release {
    tag_name: String,
}

pub(crate) async fn check_github_release(
    namespace: String,
    repository: String,
    previous_revision: &Revision,
) -> color_eyre::eyre::Result<BuildDirective> {
    let github_client = reqwest::Client::builder()
        .user_agent("release-checker (+https://git.asonix.dog/asonix/release-checker)")
        .build()?;

    let url = format!(
        "https://api.github.com/repos/{}/{}/releases",
        namespace, repository,
    );

    let releases: Vec<Release> = github_client
        .get(url)
        .header("Accept", "application/vnd.github.v3+json")
        .send()
        .await?
        .json()
        .await?;

    let opt = releases
        .iter()
        .map(|release| Version::parse_from(&release.tag_name))
        .latest_or_semver(None);

    if let Some(version) = opt {
        if version <= previous_revision.version {
            return Ok(BuildDirective::ShouldIgnore);
        }

        return Ok(BuildDirective::ShouldBuild(Some(version)));
    }

    Ok(BuildDirective::ShouldIgnore)
}

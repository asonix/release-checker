use tracing::subscriber::set_global_default;
use tracing_error::ErrorLayer;
use tracing_subscriber::layer::SubscriberExt;
use tracing_subscriber::{EnvFilter, Registry};

pub(crate) fn init_tracing() -> color_eyre::eyre::Result<()> {
    let env_filter = EnvFilter::try_from_default_env().unwrap_or_else(|_| EnvFilter::new("info"));
    let format_layer = tracing_subscriber::fmt::layer().pretty();

    let subscriber = Registry::default()
        .with(env_filter)
        .with(format_layer)
        .with(ErrorLayer::default());

    set_global_default(subscriber)?;
    Ok(())
}

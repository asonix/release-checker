mod alpine;
mod config;
mod dockerhub;
mod git;
mod gitea;
mod github;
mod init_tracing;
mod revision;
mod version;

use alpine::check_alpine_package;
use config::{CheckConfig, ProjectConfig};
use dockerhub::check_dockerhub_image;
use git::check_git_project;
use gitea::{build_client, create_tag, get_previous_revision};
use github::check_github_release;
use init_tracing::init_tracing;
use revision::Revision;
use version::Version;

enum BuildDirective {
    ShouldBuild(Option<Version>),
    ShouldIgnore,
}

async fn check_project(
    project: ProjectConfig,
    previous_revision: &Revision,
) -> color_eyre::eyre::Result<BuildDirective> {
    match project {
        ProjectConfig::Git {
            repository,
            recent,
            branch,
            language,
        } => check_git_project(repository, branch, recent, language).await,
        ProjectConfig::Alpine {
            package,
            branch,
            architectures,
        } => check_alpine_package(package, branch, architectures, previous_revision).await,
        ProjectConfig::DockerHub {
            namespace,
            repository,
            regex,
            prefix,
        } => check_dockerhub_image(namespace, repository, regex, prefix, previous_revision).await,
        ProjectConfig::Github {
            namespace,
            repository,
        } => check_github_release(namespace, repository, previous_revision).await,
        ProjectConfig::None => Ok(BuildDirective::ShouldBuild(None)),
    }
}

#[tracing::instrument]
async fn run() -> color_eyre::eyre::Result<()> {
    let config = tokio::fs::read_to_string("./Check.toml").await?;
    let config: CheckConfig = toml::from_str(&config)?;

    let client = build_client(&std::env::var("GITEA_TOKEN")?)?;

    let previous_revision = get_previous_revision(&client, &config.gitea).await?;
    tracing::info!("Previous revision: {}", previous_revision);

    let version_opt = match check_project(config.project, &previous_revision).await? {
        BuildDirective::ShouldIgnore => {
            tracing::info!("Nothing to do");
            return Ok(());
        }
        BuildDirective::ShouldBuild(version_opt) => version_opt,
    };

    let revision = previous_revision.next(version_opt);
    tracing::info!("New revision: {}", revision);

    create_tag(&client, &config.gitea, &revision).await?;

    Ok(())
}

#[tokio::main]
async fn main() -> color_eyre::eyre::Result<()> {
    init_tracing()?;
    color_eyre::install()?;

    run().await
}

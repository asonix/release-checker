use crate::{revision::Revision, version::Version, BuildDirective};
use std::collections::HashSet;

#[derive(Debug)]
enum AlpineError {
    NoPackage,
    VersionMismatch,
}

impl std::fmt::Display for AlpineError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::NoPackage => write!(f, "No package found for query"),
            Self::VersionMismatch => write!(f, "Multiple versions detected for architectures"),
        }
    }
}

impl std::error::Error for AlpineError {}

#[tracing::instrument]
pub(crate) async fn check_alpine_package(
    package: String,
    branch: String,
    architectures: Vec<String>,
    previous_revision: &Revision,
) -> color_eyre::eyre::Result<BuildDirective> {
    let alpine_client = reqwest::Client::builder()
        .user_agent("release-checker (+https://git.asonix.dog/asonix/release-checker)")
        .build()?;

    let url = format!(
        "https://pkgs.alpinelinux.org/packages?name={}&branch={}",
        package, branch
    );

    let response = alpine_client.get(url).send().await?;

    let text = response.text().await?;

    let document = scraper::Html::parse_document(&text);

    let tr_selector = scraper::Selector::parse("tr").expect("'tr' is valid selector");
    let version_selector = scraper::Selector::parse("td.version strong a")
        .expect("'td.version strong a' is valid selector");
    let arch_selector =
        scraper::Selector::parse("td.arch a").expect("'td.arch a' is valid selector");

    let mut versions = HashSet::new();

    for row in document.select(&tr_selector) {
        if let Some(arch) = row.select(&arch_selector).next() {
            if architectures
                .iter()
                .any(|architecture| architecture == arch.inner_html().trim())
            {
                if let Some(version) = row.select(&version_selector).next() {
                    versions.insert(version.inner_html().trim().to_string());
                }
            }
        }
    }

    if versions.len() > 1 {
        return Err(AlpineError::VersionMismatch.into());
    }

    if let Some(version) = versions
        .into_iter()
        .next()
        .as_deref()
        .map(Version::parse_from)
    {
        if version <= previous_revision.version {
            return Ok(BuildDirective::ShouldIgnore);
        }

        return Ok(BuildDirective::ShouldBuild(Some(version)));
    }

    Err(AlpineError::NoPackage.into())
}

use std::path::PathBuf;

#[derive(Clone, Debug, serde::Deserialize)]
#[serde(rename_all = "kebab-case")]
pub(crate) struct CheckConfig {
    pub(crate) project: ProjectConfig,
    pub(crate) gitea: GiteaConfig,
}

#[derive(Clone, Debug, serde::Deserialize)]
#[serde(rename_all = "kebab-case")]
#[serde(tag = "kind")]
pub(crate) enum ProjectConfig {
    None,

    #[serde(rename = "github")]
    Github {
        namespace: String,
        repository: String,
    },

    #[serde(rename = "dockerhub")]
    DockerHub {
        namespace: String,
        repository: String,
        regex: String,
        prefix: Option<String>,
    },

    #[serde(rename = "alpine")]
    Alpine {
        package: String,
        branch: String,
        architectures: Vec<String>,
    },

    #[serde(rename = "git")]
    Git {
        repository: String,
        branch: String,
        recent: i64,

        #[serde(flatten)]
        language: LanguageConfig,
    },
}

#[derive(Clone, Debug, serde::Deserialize)]
#[serde(rename_all = "kebab-case")]
#[serde(tag = "language")]
pub(crate) enum LanguageConfig {
    #[serde(rename = "rust")]
    Rust { config: PathBuf },
}

#[derive(Clone, Debug, serde::Deserialize)]
#[serde(rename_all = "kebab-case")]
pub(crate) struct GiteaConfig {
    pub(crate) domain: String,
    pub(crate) owner: String,
    pub(crate) repo: String,
}
